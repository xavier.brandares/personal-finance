import React from 'react';
import '../css/App.css';
import Routes from '../components/routes/Routes';
import { BrowserRouter } from 'react-router-dom';

function App() {
return (
	<div className="App">
		<BrowserRouter>
			<Routes/>
		</BrowserRouter>
	</div>
);
}

export default App;
