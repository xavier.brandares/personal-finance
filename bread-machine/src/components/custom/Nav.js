import React, { Component } from 'react';
import history from '../routes/history';
import '../../css/custom/Nav.css';


export default class Nav extends Component{
    render(){
        return(
            <div className='nav-container'>
                <button onClick={() => history.push('/')} className='nav-button'>Home</button>
                <button onClick={() => history.push('/projections')} className='nav-button'>Projections</button>
            </div>
        )
    }
}