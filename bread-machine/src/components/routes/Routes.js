import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import Landing from '../pages/Landing';
import Projections from '../pages/Projections';
import history from './history';

export default class Routes extends Component {
    render(){
        return(
            <Router history={history}>
                <Switch>
                    <Route path='/' exact component={Landing} />
                    <Route path='/projections' exact component={Projections} />
                </Switch>
            </Router>
        )
    }
}