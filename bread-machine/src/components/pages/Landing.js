import React, { Component } from 'react';
import Nav from '../custom/Nav';
import '../../css/pages/Landing.css';

class Landing extends Component{
    render(){
        return(
            <div>
                <Nav />
                <div className="landing-header">
                    <img alt='' className="landing-image" src="https://firebasestorage.googleapis.com/v0/b/personalynkprototype.appspot.com/o/bread-machine.jpg?alt=media&token=03b550c4-5895-4875-9bc2-1643ece49d71" />
                    <p>Welcome to the Bread Machine Personal Finance Project</p>
                    <a
                        className="landing-link"
                        href="https://gitlab.com/xavier.brandares/personal-finance"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        contribute to the project
                    </a>
                </div>
            </div>
        )
    }
}

export default Landing;