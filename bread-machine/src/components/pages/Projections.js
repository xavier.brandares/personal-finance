import React, { Component } from 'react';
import Nav from '../custom/Nav';
import '../../css/pages/Projections.css';

class Projections extends Component{
    render(){
        return(
            <div>
                <Nav />
                <div className='projections-body'>
                    <p>
                        Projections Page
                    </p>
                </div>
            </div>
        )
    }
}

export default Projections;